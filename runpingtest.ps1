$result = Test-Connection 8.8.8.8 -Count 1 -ErrorAction SilentlyContinue -ErrorVariable PingError -TimeToLive 255;

if($PingError){
    $result = Test-Connection 8.8.4.4 -Count 1 -ErrorAction SilentlyContinue -ErrorVariable PingError -TimeToLive 255;
}

if($PingError){
    $ping = 0;
} else {
    $ping = $result.ResponseTime;
}

Write-Host "$(Get-Date)`t$($Ping)";
"$(Get-Date)`t$($Ping)" | Out-File "C:\Users\kohbo\Coding\Internet_Reliability_Python\results_new.csv" -Append