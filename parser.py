import datetime
import csv

records = []
down_stats = {
    "time_between_down": 0,
    "down_occurences": 0,
    "time_spent_down": 0,
}

with open(r"C:\Users\kohbo\Coding\Internet_Reliability_Python\results_new.csv", encoding='utf-8') as datefile:
    rdr = csv.reader((line.replace("\0", "") for line in datefile), delimiter='\t')
    first = None
    prev = None
    down_start = None
    for l in rdr:
        if len(l) != 2:
            continue
        ping = int(l[1])
        dt = datetime.datetime.strptime(l[0], "%m/%d/%Y %H:%M:%S")
        if first is None:
            first = dt
        if ping == 0:
            if not down_start:
                down_start = dt
            if prev:
                delta = (dt - prev).total_seconds()
                if delta > 90:
                    down_stats["time_between_down"] += delta
                    down_stats["down_occurences"] += 1
            prev = dt
        else:
            if down_start:
                down_stats["time_spent_down"] += (dt - down_start).total_seconds()
            down_start = None

print("Over {} days:".format((prev-first).days))
print("Average Time Without Internet: {0:.2f} minutes".format(down_stats["time_spent_down"]/down_stats["down_occurences"]/60))
print("Average Time Between Disconnects: {0:.2f} minutes".format(down_stats["time_between_down"]/down_stats["down_occurences"]/60))
